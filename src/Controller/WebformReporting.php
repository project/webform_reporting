<?php

namespace Drupal\webform_reporting\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\webform\Entity\Webform;
use Drupal\webform_reporting\Form\WebformReport;

/**
 *
 */
class WebformReporting extends ControllerBase {

  /**
   */
  public function listReports(Webform $webform) {
    $reports = $webform->getThirdPartySettings('webform_reporting') ?: [];

    $frequencyList = WebformReport::getFrequencyList();

    $rows = array_map(function($item) use ($webform, $frequencyList){
      return [
        ['data' => $item['report_info']['title'] ?? ''],
        ['data' => $item['report_info']['notes'] ?? ''],
        ['data' => $item['report_info']['frequency']
            ? (
              $item['report_info']['frequency'] === 'custom'
              ? sprintf('%s (Custom)', $item['report_info']['frequency_custom'])
              : $frequencyList[$item['report_info']['frequency']]
            )
            : ''
        ],
        ['data' => ($item['report_info']['enabled'] ?? false) ?  $this->t('Enabled') : $this->t('Disabled')],
        ['data' => [
          '#type' => 'dropbutton',
          '#links' => [
            'edit' => [
              'title' => $this->t("Edit"),
              'url' => Url::fromRoute('entity.webform.reporting_edit', [
                'webform' => $webform->id(),
                'id' => $item['report_info']['machine_name'],
              ]),
            ],
            'delete' => [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.webform.reporting_remove', [
                'webform' => $webform->id(),
                'id' => $item['report_info']['machine_name'],
              ]),
            ]
          ]
        ]],
      ];
    }, array_values($reports));

    return [
      'description' => [
        '#markup' => '<p>' . $this->t('Send timely reports') . '</p>',
        'add' => [
          '#prefix' => '<p>',
          '#suffix' => '</p>',
          '#type' => 'link',
          '#title' => $this->t('Add Report'),
          '#url' => Url::fromRoute('entity.webform.reporting_add', [ 'webform' => $webform->id() ]),
          '#attributes' => [
            'class' => ['button', 'button-action', 'button--small']
          ]
        ],
      ],
      'table' => [
        '#type' => 'table',
        '#header' => [
          $this->t('Title'),
          ['data' => $this->t('Description'), 'width' => '50%'],
          ['data' => $this->t('Frequency'), 'width' => '10%'],
          ['data' => $this->t('Status'), 'width' => '10%'],
          ['data' => $this->t('Operations'), 'width' => '10%'],
        ],
        '#rows' => $rows,
        '#empty' => $this->t('No reports found.'),
      ]
    ];
  }

}
