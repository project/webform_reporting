<?php

namespace Drupal\webform_reporting\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * @Annotation
 */
class WebformReportingExport extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the webform handler.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A brief description of the webform reporting plugin.
   *
   * This will be shown when configuring the report.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description = '';

}
