<?php

namespace Drupal\webform_reporting\Plugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginBase;

/**
 *
 */
abstract class WebformReportingExportBase extends PluginBase implements WebformReportingExportInterface {

  /**
   * {@inheritdoc}
   */
  public function form($form, FormStateInterface $formState) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($form, FormStateInterface $formState) {}

  /**
   * {@inheritdoc}
   */
  public function submit($form, FormStateInterface $formState) {}

  /**
   * Export Report
   */
  public function export() {
    return false;
  }
}
