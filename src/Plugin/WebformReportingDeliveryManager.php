<?php

namespace Drupal\webform_reporting\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\webform_reporting\Annotation\WebformReportingDelivery;

class WebformReportingDeliveryManager extends DefaultPluginManager {

  /**
   * The configuration object factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a WebformReportingDeliveryManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration object factory.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory) {
    parent::__construct(
      'Plugin/WebformReportingDelivery',
      $namespaces,
      $module_handler,
      'Drupal\webform_reporting\Plugin\WebformReportingDeliveryInterface',
      'Drupal\webform_reporting\Annotation\WebformReportingDelivery'
    );
    $this->configFactory = $config_factory;

    $this->alterInfo('webform_reporting_delivery_info');
    $this->setCacheBackend($cache_backend, 'webform_reporting_delivery_plugins');
  }

  public function getDefinitions()
  {
    $definitions = parent::getDefinitions();

    uasort($definitions, function($a, $b) {
      if ($a['weight'] == $b['weight']) return 0;
      return ($a['weight'] < $b['weight']) ? -1 : 1;
    });

    return $definitions;
  }

}
