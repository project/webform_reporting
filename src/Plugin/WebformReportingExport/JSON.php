<?php

namespace Drupal\webform_reporting\Plugin\WebformReportingExport;

use Drupal\webform_reporting\Annotation\WebformReportingExport;
use Drupal\webform_reporting\Plugin\WebformReportingExportBase;

/**
 * @WebformReportingExport(
 *   id = "json",
 *   label = @Translation("JSON")
 * )
 */
class JSON extends WebformReportingExportBase {

}
