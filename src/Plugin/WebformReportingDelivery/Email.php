<?php

namespace Drupal\webform_reporting\Plugin\WebformReportingDelivery;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform_reporting\Annotation\WebformReportingDelivery;
use Drupal\webform_reporting\Plugin\WebformReportingDeliveryBase;

/**
 *
 * @WebformReportingDelivery(
 *   id = "email",
 *   label = @Translation("Email"),
 *   description = @Translation("Used to send reports through email to a particular location.")
 * )
 */
class Email extends WebformReportingDeliveryBase {

  /**
   * @param $form
   * @param FormStateInterface $formState
   * @return array
   */
  public function form($form, FormStateInterface $formState) {
    $form['from_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('From Name'),
      '#description' => $this->t('The name this report is coming from'),
      '#default_value' => $this->configuration['from_name'] ?: '',
    ];

    $form['from_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('From Email'),
      '#description' => $this->t('The email address this report is coming from'),
      '#default_value' => $this->configuration['from_email'] ?: '',
    ];

    $form['to'] = [
      '#type' => 'textfield',
      '#title' => $this->t('To'),
      '#required' => true,
      '#description' => $this->t('The email addresse(es) the following report should be sent to. For multiple emails use comma delimited.'),
      '#default_value' => $this->configuration['to'] ?: '',
    ];

    $form['reply_to'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reply To Email'),
      '#description' => $this->t(''),
      '#default_value' => $this->configuration['reply_to'] ?: '',
    ];

    $form['cc'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CC'),
      '#description' => $this->t('The email addresse(es) the following report should be sent to as a CC (Carbon Copy). For multiple emails use comma delimited.'),
      '#default_value' => $this->configuration['cc'] ?: '',
    ];

    $form['bcc'] = [
      '#type' => 'textfield',
      '#title' => $this->t('BCC'),
      '#description' => $this->t('The email addresse(es) the following report should be sent to as a BCC (Blind Carbon Copy). For multiple emails use comma delimited.'),
      '#default_value' => $this->configuration['bcc'] ?: '',
    ];

    $form['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#description' => $this->t('Title of the email of these reports'),
      '#required' => true,
      '#default_value' => $this->configuration['subject'] ?: '',
    ];

    $form['body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#description' => $this->t('Body of the email'),
      '#default_value' => $this->configuration['body'] ?: '',
    ];

    return $form;
  }

}
