<?php

namespace Drupal\webform_reporting\Plugin\WebformReportingDelivery;

use Drupal\webform_reporting\Annotation\WebformReportingDelivery;
use Drupal\webform_reporting\Plugin\WebformReportingDeliveryBase;

/**
 * @WebformReportingDelivery(
 *   id = "no_delivery",
 *   label = @Translation("No Delivery"),
 *   description = @Translation("Nothing happens after export with this."),
 *   weight = -1
 * )
 */
class NoDelivery extends WebformReportingDeliveryBase {

}
