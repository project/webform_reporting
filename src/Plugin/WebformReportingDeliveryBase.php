<?php

namespace Drupal\webform_reporting\Plugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginBase;

/**
 * Abstract Class WebformReportingDeliveryBase
 * @package Drupal\webform_reporting
 */
abstract class WebformReportingDeliveryBase extends PluginBase implements WebformReportingDeliveryInterface {

  /**
   * {@inheritdoc}
   */
  public function form($form, FormStateInterface $formState) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($form, FormStateInterface $formState) {}

  /**
   * {@inheritdoc}
   */
  public function submit($form, FormStateInterface $formState) {}

  /**
   * Send Report
   */
  public function send() {
    return true;
  }

}
