<?php

namespace Drupal\webform_reporting\Plugin;

use Drupal\Core\Form\FormStateInterface;

/**
 * Interface WebformReportingExportInterface
 */
interface WebformReportingExportInterface {

  public function form($form, FormStateInterface $formState);
  public function validate($form, FormStateInterface $formState);
  public function submit($form, FormStateInterface $formState);
  public function export();

}
