<?php

namespace Drupal\webform_reporting\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\webform\Entity\Webform;

/**
 * Class WebformReportDeleteConfirm
 */
class WebformReportDeleteConfirm extends ConfirmFormBase {

  /**
   * @var Webform
   */
  protected $webform;

  /**
   * ID of the item to delete.
   *
   * @var int
   */
  protected $id;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Webform $webform = NULL, string $id = NULL) {
    $this->webform = $webform;
    $this->id = $id;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->webform->unsetThirdPartySetting('webform_reporting', $this->id);
    $this->webform->save();
    $form_state->setRedirect('entity.webform.reporting_lists', ['webform' => $this->webform->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "webform_report_delete_confirm_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.webform.reporting_lists', ['webform' => $this->webform->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $settings = $this->webform->getThirdPartySetting('webform_reporting', $this->id);
    return $this->t('Do you want to delete report %title?', ['%title' => $settings['report_info']['title']]);
  }
}
