<?php

namespace Drupal\webform_reporting\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\job_scheduler\JobSchedulerInterface;
use Drupal\webform\Entity\Webform;
use Drupal\webform_reporting\Plugin\WebformReportingDeliveryBase;
use Drupal\webform_reporting\Plugin\WebformReportingDeliveryManager;
use Drupal\webform_reporting\Plugin\WebformReportingExportBase;
use Drupal\webform_reporting\Plugin\WebformReportingExportManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class WebformReport extends FormBase {

  protected $webformReportingDeliveryManager;

  protected $webformReportingExportManager;

  protected $jobScheduler;

  protected $reportKeys = ['report_info', 'report_delivery', 'report_export', 'report_conditions'];

  /**
   *
   */
  public function __construct(
    WebformReportingDeliveryManager $webformReportingDeliveryManager,
    WebformReportingExportManager $webformReportingExportManager,
    JobSchedulerInterface $jobScheduler
  ) {
    $this->webformReportingDeliveryManager = $webformReportingDeliveryManager;
    $this->webformReportingExportManager = $webformReportingExportManager;
    $this->jobScheduler = $jobScheduler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.webform_reporting_delivery'),
      $container->get('plugin.manager.webform_reporting_export'),
      $container->get('job_scheduler.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_report_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Webform $webform = null, $id = '') {
    $data = !empty($form_state->getUserInput()) ? $form_state->getUserInput() : [];

    if (!empty($id) && empty($data)) {
      $data = $webform->getThirdPartySetting('webform_reporting', $id);
    }

    $form['webform'] = [
      '#type' => 'value',
      '#value' => $webform
    ];

    $form['report_id'] = [
      '#type' => 'value',
      '#value' => $id,
    ];

    $form['report_info'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Report Information'),
      '#tree' => true,
    ];

    $form['report_info']['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Report Title'),
      '#required' => true,
      '#default_value' => $data['report_info']['title'] ?? '',
    ];

    $form['report_info']['machine_name'] = [
      '#type' => 'machine_name',
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'source' => ['report_info', 'title']
      ],
      '#default_value' => $data['report_info']['machine_name'] ?? '',
//      '#disabled' => $data['report_info']['machine_name'] ?? false,
    ];

    $form['report_info']['notes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Administrative Notes'),
      '#default_value' => $data['report_info']['notes'] ?? '',
    ];

    $form['report_info']['frequency'] = [
      '#type' => 'select',
      '#title' => $this->t('Frequency'),
      '#required' => true,
      '#options' => self::getFrequencyList(),
      '#default_value' => $data['report_info']['frequency'] ?? '',
    ];

    $form['report_info']['frequency_custom'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom Frequency'),
      '#description' => $this->t('The following should be written in cron notation. For more info see <a target="_blank" href="https://crontab.guru/">crontab.guru</a>.'),
      '#states' => [
        'visible' => [
          '[name="report_info[frequency]"]' => ['value' => 'custom'],
        ],
        'required' => [
          '[name="report_info[frequency]"]' => array('value' => 'custom'),
        ],
      ],
      '#default_value' => $data['report_info']['frequency_custom'] ?? '',
    ];

    $form['report_info']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $data['report_info']['enabled'] ?? false,
    ];

    $form['report_delivery'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Delivery'),
      '#tree' => true,
    ];

    $plugins = $this->webformReportingDeliveryManager->getDefinitions();

    $deliveryPlugins = [ NULL => $this->t('- Select -') ] + array_map(function($item){
      return $item['label'];
    }, $plugins);

    $form['report_delivery']['plugin'] = [
      '#type' => 'select',
      '#required' => true,
      '#title' => $this->t('Plugin'),
      '#options' => $deliveryPlugins,
      '#ajax' => [
        'callback' => '::pluginConfigForm',
        'wrapper' => 'plugin-config-form',
      ],
      '#default_value' => $data['report_delivery']['plugin']  ?? '',
    ];

    $form['report_delivery']['config'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'plugin-config-form'],
    ];

    if (!empty($data['report_delivery']['plugin'])) {
      $pluginConfig = $data['report_delivery']['config'] ?? [];
      /** @var WebformReportingDeliveryBase $plugin */
      $plugin = $this->webformReportingDeliveryManager->createInstance($data['report_delivery']['plugin'], $pluginConfig);
      $form['report_delivery']['config'] = $plugin->form($form['report_delivery']['config'], $form_state);
    }

    $form['report_export'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Export'),
      '#tree' => true,
    ];

    $exportPlugins = $this->webformReportingExportManager->getDefinitions();

    $exportPlugins =
      [NULL => $this->t('- Select -')] +
      array_map(function($item){
        return $item['label'];
      }, $exportPlugins);

    $form['report_export']['plugin'] = [
      '#type' => 'select',
      '#required' => true,
      '#title' => $this->t('Plugin'),
      '#options' => $exportPlugins,
      '#ajax' => [
        'callback' => '::exportConfigForm',
        'wrapper' => 'export-config-form',
      ],
      '#default_value' => $data['report_export']['plugin'] ?? '',
    ];

    $form['report_export']['config'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'export-config-form'],
    ];

    if (!empty($data['report_export']['plugin'])) {
      $pluginConfig = $data['report_export']['config'] ?? [];
      /** @var WebformReportingExportBase $plugin */
      $plugin = $this->webformReportingExportManager->createInstance($data['report_export']['plugin'], $pluginConfig);
      $form['report_export']['config'] = $plugin->form($form['report_export']['config'], $form_state);
    }

    $form['report_conditions'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Conditions'),
      '#tree' => true,
      '#attributes' => ['id' => 'conditions']
    ];

    $form['report_conditions']['help'] = [
      '#markup' => '<p>' . $this->t('Use the following fields to filter the report by specific criteria.') . '</p>',
    ];

    $form['report_conditions']['conditions'] = [
      '#type' => 'table',
      '#title' => $this->t('Conditions'),
      '#description' => $this->t('The following identifies what is needed for each query'),
      '#header' => [
        $this->t('Field'),
        $this->t('Condition'),
        $this->t('Value'),
        '' # Operations Column
      ],
    ];

    $fields = array_merge(
      [NULL => $this->t('- Select -')],
      $this->getFields($webform)
    );

    $conditions = $data['report_conditions']['conditions'] ?? [];
    foreach ( $conditions AS $i => $condition) {
      $form['report_conditions']['conditions'][$i]['field'] = [
        '#type' => 'select',
        '#options' => $fields,
        '#default_value' => $condition['field'] ?? '',
      ];

      $form['report_conditions']['conditions'][$i]['condition'] = [
        '#type' => 'select',
        '#options' => [
          NULL => $this->t('- Select -'),
          '=' => $this->t('Equal'),
          '!=' => $this->t('Not Equal'),
          '>' => $this->t('Greater Than'),
          '>=' => $this->t('Greater Than Equal'),
          '<' => $this->t('Less Than'),
          '<=' => $this->t('Less Than Equal'),
          'IN' => $this->t('In'),
          'LIKE' => $this->t('Contains')
        ],
        '#default_value' => $condition['condition'] ?? '',
      ];

      $form['report_conditions']['conditions'][$i]['value'] = [
        '#type' => 'textfield',
        '#default_value' => $condition['value'] ?? '',
      ];

      $form['report_conditions']['conditions'][$i]['remove'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#name' => 'remove_' . $i,
        '#data-id' => $i,
        '#ajax' => [
          'callback' => '::conditionRow',
          'wrapper' => 'conditions',
        ],
        '#submit' => ['::removeConditionRow'],
        '#limit_validation_errors' => [],
      ];
    }

    $form['report_conditions']['add_more'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add Another Condition'),
      '#ajax' => [
        'callback' => '::conditionRow',
        'wrapper' => 'conditions',
      ],
      '#submit' => ['::addConditionRow'],
      '#limit_validation_errors' => [],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save Report'),
      '#attributes' => [
        'class' => ['button--primary']
      ]
    ];

    $form['actions']['cancel'] = [
      '#type' => 'button',
      '#value' => $this->t('Cancel'),
      '#submit' => ['::cancel'],
      '#limit_validation_errors' => [],
    ];

    if (!empty($id)) {
      $form['actions']['delete'] = [
        '#type' => 'link',
        '#title' => $this->t('Delete'),
        '#url' => Url::fromRoute('entity.webform.reporting_remove', ['webform' => $webform->id(), 'id' => $id]),
        '#attributes' => [
          'class' => ['button', 'button--danger']
        ]
      ];
    }

    return $form;
  }

  /**
   * Cancel Callback
   */
  public function cancel(array $form, FormStateInterface $form_state) {
    /** @var Webform $webform */
    $webform = $form_state->getBuildInfo()['args'][0];
    $form_state->setRedirect('entity.webform.results_email', ['webform' => $webform->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $cleaned_values = $form_state->cleanValues();

    /** @var Webform $webform */
    $webform = $form_state->getValue('webform');

    $data = [];
    foreach ($this->reportKeys AS $key) {
      $data[$key] = $cleaned_values->getValue($key);
    }

    $webform->setThirdPartySetting('webform_reporting', $data['report_info']['machine_name'], $data);
    $webform->save();

    $frequency = $form_state->getValue('report_info')['frequency'] ?? '';
    $frequency_other = $form_state->getValue('report_info')['frequency_custom'] ?? '';

    if ($frequency !== 'manual') {
      $cron = $frequency === 'custom' ? $frequency_other : $frequency;

      $job = [
        'name' => 'webform_reporting',
        'type' => $webform->id(),
        'id' => 0,
        'data' => [
          'report_id' => $data['report_info']['machine_name'],
        ],
        'crontab' => $cron,
        'periodic' => TRUE
      ];
      $exists = $this->jobScheduler->check($job);
      if (!$exists) {
        $this->jobScheduler->set($job);
      }
    }

    $form_state->setRedirect('entity.webform.reporting_lists', ['webform' => $webform->id()]);
  }

  /**
   * Ajax callback to return report delivery form.
   */
  public function pluginConfigForm($form, FormStateInterface $formState) {
    return $form['report_delivery']['config'];
  }

  /**
   * Ajax callback to return report export form.
   */
  public function exportConfigForm($form, FormStateInterface $formState) {
    return $form['report_export']['config'];
  }

  /**
   * Ajax callback to return condition fields.
   */
  public function conditionRow($form, FormStateInterface $formState) {
    return $form['report_conditions'];
  }

  /**
   * Ajax callback used to add field for the conditions.
   */
  public function addConditionRow(&$form, FormStateInterface $form_state) {
    $data = $form_state->get('report_data') ?? [];
    $data['report_conditions']['conditions'][] = [];
//    $form_state->set('report_data', $data);
    $form_state->setRebuild();
  }

  /**
   * Ajax callback used for removing field from condition.
   */
  public function removeConditionRow(&$form, FormStateInterface $form_state) {
    $data = $form_state->get('report_data') ?? [];

    $removeButton = $form_state->getTriggeringElement();
    if (isset($removeButton['#data-id'])) {
      $data_id = $removeButton['#data-id'];
      unset($data['report_conditions']['conditions'][$data_id]);
    }

//    $form_state->set('report_data', $data);
    $form_state->setRebuild();
  }

  /**
   *
   */
  public function exists($id) {
    return false;
  }

  /**
   * Return a list of all of the avilable fields used for this wo
   */
  private function getFields(Webform $webform) {
    $fieldInfo = [];

    $fieldInfo[NULL] = $this->t('- Select -');
    // Core Webform Fields
    // @ToDo: Is there a place I can pull these?
    $fieldInfo['Webform Submission']['ws.sid'] = $this->t('Submission ID');
    $fieldInfo['Webform Submission']['ws.webform_id'] = $this->t('Webform ID');
    $fieldInfo['Webform Submission']['ws.uuid'] = $this->t('Unique ID');
    $fieldInfo['Webform Submission']['ws.langcode'] = $this->t('Language');
    $fieldInfo['Webform Submission']['ws.serial'] = $this->t('Serial');
    $fieldInfo['Webform Submission']['ws.token'] = $this->t('Token');
    $fieldInfo['Webform Submission']['ws.uri'] = $this->t('Url Where Submitted');
    $fieldInfo['Webform Submission']['ws.created'] = $this->t('Created Date/Time');
    $fieldInfo['Webform Submission']['ws.completed'] = $this->t('Completed Date/Time');
    $fieldInfo['Webform Submission']['ws.changed'] = $this->t('Changed Date/Time');
    $fieldInfo['Webform Submission']['ws.in_draft'] = $this->t('In Draft');
    $fieldInfo['Webform Submission']['ws.current_page'] = $this->t('Current Page');
    $fieldInfo['Webform Submission']['ws.remote_addr'] = $this->t('Remote Address');
    $fieldInfo['Webform Submission']['ws.uid'] = $this->t('User ID');
    $fieldInfo['Webform Submission']['ws.entity_type'] = $this->t('Entity Type');
    $fieldInfo['Webform Submission']['ws.entity_id'] = $this->t('Entity ID');
    $fieldInfo['Webform Submission']['ws.locked'] = $this->t('Locked');
    $fieldInfo['Webform Submission']['ws.sticky'] = $this->t('Sticky');
    $fieldInfo['Webform Submission']['ws.notes'] = $this->t('Notes');

    // Form Fields
    $fields = $webform->getElementsDecodedAndFlattened();

    foreach ($fields AS $field_key => $field_item) {
      $fieldInfo['Webform Submission Data']['wsd.' . $field_key] = $field_item['#title'] ?: $field_key;
    }

    return $fieldInfo;
  }

  /**
   * @return array
   */
  public static function getFrequencyList() {
    $list = [
      'manual' => t('Manual'),
      '0 * * * *' => t('Hourly'),
      '0 0 * * *' => t('Daily'),
      '0 0 * * 0' => t('Weekly'),
      '0 0 1 * *' => t('Monthly'),
      '0 0 1 1 *' => t('Yearly'),
      'custom' => t('Custom'),
    ];

    \Drupal::moduleHandler()->alter('webform_module_frequency', $list);

    return $list;
  }
}
